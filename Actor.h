#ifndef ACTOR_H
#define ACTOR_H

#include "Helpers.h"
#include "SpriteManager.h"

// general actor class which describes what & how actors can do on the scene
class Actor{
protected:
	int				m_spriteID = 0;
	Vector2			m_lastPosition;
	Vector2			m_position;
	Vector2			m_velocity;
	Vector2			m_sizes;
	bool			m_flippedVisual = false;

public:

	Actor(int spriteID, int w, int h);

	Vector2			GetPosition(){ return m_position; }
	Vector2			GetCenter(){ return (m_position + m_sizes*0.5f); }
	Vector2			GetSizes(){ return m_sizes; }
	Vector2			GetVelocity(){ return m_velocity; }

	void			SetVelocity(Vector2 velocity){ m_velocity = velocity; }
	void			FlipVisual(bool state){ m_flippedVisual = state; }

	virtual void	Update();
	void			UpdatePosition(Vector2 newPosition, bool zeroVelocity = false);
	void			Draw(SpriteManager* sm);
	bool			Collides(Actor &other, CollisionType* type);
	void			HandleCollision(Actor* other);
};


#endif