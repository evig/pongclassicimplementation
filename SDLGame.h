#ifndef SDL_GAME_H
#define SDL_GAME_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <vector>
#include <time.h>

#include "Helpers.h"
#include "Actor.h"
#include "SpriteManager.h"

// Base sdl game class which describes general game's lifecycle
// You might want to inherit from this class to create your own game ;-)
class SDLGame{
protected:
	SpriteManager*	m_sprites;
	SDL_Window*		m_wnd;
	SDL_Renderer*	m_renderer;
	SDL_Texture*	m_texture;
	TTF_Font*		m_font;

	bool					g_done = false;
	Vector2					g_mousePosition;
	std::vector<Actor*>		m_actors;

public:

	SDLGame(SpriteManager* sm);
	bool			Initialize();
	void			UpdateLogic();
	void			AfterUpdateLogic();
	void			Draw();
	void			AfterDraw();
	bool			GameLoop();
	void			CleanUp();
};



#endif