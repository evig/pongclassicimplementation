#ifndef BALL_H
#define BALL_H

#include "Helpers.h"
#include "Actor.h"

// Simple ball class, special type of actor
class Ball : public Actor{
protected:

public:
	Ball(int spriteID, int w, int h);

	void Reset(bool initVelocity = false);
	// update method implements ball's bounces
	void Update();
};

#endif