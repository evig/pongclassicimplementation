#include "Helpers.h"



int Helper::random(int from, int to){
	return from + rand() % (to - from);
}


int Helper::common(int p1x1, int p1x2, int p2x1, int p2x2){
	int len = p1x2 - p1x1;

	if (p2x1 > p1x1)
		len -= (p2x1 - p1x1);

	if (p2x2 < p1x2)
		len += (p2x2 - p1x2);

	return len;
}


Vector2::Vector2(){
	this->X = 0;
	this->Y = 0;
}
Vector2::Vector2(float x, float y){
	this->X = x;
	this->Y = y;
}

float Vector2::Length(){
	return sqrt(this->X*this->X + this->Y*this->Y);
}

Vector2 Vector2::Normalized(){
	float len = this->Length();
	if (len == 0)
		len = 1;

	return Vector2(this->X / len, this->Y / len);
}




void operator += (Vector2 &a, Vector2 &b){
	a.X += b.X;
	a.Y += b.Y;
}
Vector2 operator - (Vector2 &a, Vector2 &b){
	return Vector2(a.X - b.X, a.Y - b.Y);
}
void operator -= (Vector2 &a, Vector2 &b){
	a.X -= b.X;
	a.Y -= b.Y;
}
Vector2 operator + (Vector2 &a, Vector2 &b){
	return Vector2(a.X + b.X, a.Y+b.Y);
}
Vector2 operator * (Vector2 &a, int value){
	return Vector2(a.X*value, a.Y*value);
}
void operator *= (Vector2 &a, float value){
	a.X *= value;
	a.Y *= value;
}