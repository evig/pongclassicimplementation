#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <string>

#include "Helpers.h"
#include "Ball.h"
#include "Enemy.h"
#include "SDLGame.h"




/*
	This is the main class of the game. Inheriting from base game class, implements functionality required for a simple pong game.
*/
class PongGame : public SDLGame{
protected:
	// Handles to main game objects
	Actor*		m_player;
	Ball*		m_ball;
	Actor*		m_enemy;
	
	// Scores
	int			PlayerPoints = 0;
	int			EnemyPoints = 0;

public:

	// Game lifecycle methods
				PongGame(SpriteManager* sm);
	bool		Initialize();
	void		UpdateLogic();
	void		Draw();
	bool		GameLoop();
};

PongGame::PongGame(SpriteManager* sm)
:SDLGame(sm){

}
bool PongGame::Initialize(){
	SDLGame::Initialize();

	m_sprites = new SpriteManager(m_renderer, m_texture);

	// Prepare sprites info
	// in perfect world this should be loaded from a file ;-)
	m_sprites->AddSpriteData(616, 2, 76, 78);
	m_sprites->AddSpriteData(362, 896, 194, 80);
	m_sprites->AddSpriteData(166, 896, 194, 50);
	m_sprites->AddSpriteData(2, 2, 612, 378);
	m_sprites->AddSpriteData(2, 382, 512, 512);
	m_sprites->AddSpriteData(166, 978, 114, 38);
	m_sprites->AddSpriteData(2, 896, 162, 92);


	// Initialize the game scene

	this->m_player = new Actor(1, m_sprites->GetSpriteData(1).w, m_sprites->GetSpriteData(1).h);
	this->m_ball = new Ball(0, m_sprites->GetSpriteData(0).w, m_sprites->GetSpriteData(0).h);
	this->m_enemy = new Enemy(2, m_sprites->GetSpriteData(2).w, m_sprites->GetSpriteData(2).h, m_ball);

	Actor* grass1 = new Actor(4, 0, 0);
	Actor* grass2 = new Actor(4, 0, 0);
	Actor* grass3 = new Actor(4, 0, 0);
	Actor* grass4 = new Actor(4, 0, 0);
	Actor* markings1 = new Actor(3, 0, 0);
	Actor* markings2 = new Actor(3, 0, 0);
	Actor* logo = new Actor(6, 0, 0);
	markings2->FlipVisual(true);

	grass2->UpdatePosition(Vector2(512, 0), true);
	grass3->UpdatePosition(Vector2(512, 512), true);
	grass4->UpdatePosition(Vector2(0, 512), true);
	markings1->UpdatePosition(Vector2(96, 0), true);
	markings2->UpdatePosition(Vector2(96, WINDOW_HEIGHT-378), true);

	this->m_ball->UpdatePosition(Vector2(WINDOW_WIDTH*0.5f, WINDOW_HEIGHT*0.5f), true);


	m_actors.push_back(grass1);
	m_actors.push_back(grass2);
	m_actors.push_back(grass3);
	m_actors.push_back(grass4);

	m_actors.push_back(logo);

	m_actors.push_back(markings1);
	m_actors.push_back(markings2);

	m_actors.push_back(m_player);
	m_actors.push_back(m_ball);
	m_actors.push_back(m_enemy);

	return true;
}

// Logical frame of the game
void PongGame::UpdateLogic(){

	SDLGame::UpdateLogic();

	// update all actors
	for (size_t i = 0; i < m_actors.size(); i++)
	{
		m_actors[i]->Update();
	}

	// player follows mouse cursor
	m_player->UpdatePosition(g_mousePosition);

	// the game requires only two basic collisions to work
	m_player->HandleCollision(m_ball);
	m_enemy->HandleCollision(m_ball);

	// rules of the game
	if (m_ball->GetCenter().Y < 0){
		PlayerPoints++;
		m_ball->Reset();
	}

	if (m_ball->GetCenter().Y > WINDOW_HEIGHT){
		EnemyPoints++;
		m_ball->Reset();
	}
}

void PongGame::Draw(){
	SDLGame::Draw();

	SDL_RenderClear(m_renderer);

	// redraw whole scene
	for (size_t i = 0; i < m_actors.size(); i++)
	{
		m_actors[i]->Draw(m_sprites);
	}

	// put some info on the screen
	// UI done in "no to beautiful way", but functional
	SDL_Rect res;
	res.x = 300;
	res.y = 320;
	res.w = 120;
	res.h = 40;
	SDL_Color clr = { 255, 255, 255 };
	std::string result = "Score: ";

	result.append(std::to_string(PlayerPoints));
	result.append(" : ");
	result.append(std::to_string(EnemyPoints));

	SDL_Surface* txt = TTF_RenderText_Blended(m_font, result.c_str(), clr);
	SDL_Texture *fontTexture = SDL_CreateTextureFromSurface(m_renderer, txt);
	SDL_RenderCopy(m_renderer, fontTexture, NULL, &res);

	res.x -= 20;
	res.y += 40;
	res.w = 280;
	txt = TTF_RenderText_Blended(m_font, "Hit Space to Start/Reset game!", clr);
	fontTexture = SDL_CreateTextureFromSurface(m_renderer, txt);
	SDL_RenderCopy(m_renderer, fontTexture, NULL, &res);

	SDL_RenderPresent(m_renderer);
}

// Game loop with events handling & frame limiting
bool PongGame::GameLoop(){
	while (!g_done){

		SDL_Event e;
		while (SDL_PollEvent(&e)){
			if (e.type == SDL_QUIT)
				g_done = true;

			if (e.type == SDL_KEYDOWN)
				if (e.key.keysym.sym == SDLK_SPACE)
					m_ball->Reset(true);

			if (e.type == SDL_MOUSEMOTION){
				g_mousePosition = Vector2(e.motion.x, WINDOW_HEIGHT - m_player->GetSizes().Y);
			}
		}

		UpdateLogic();
		Draw();

		SDL_Delay(1000 / FRAMES);
	}

	return true;
}

// main entry point. init & run the game
int main(int argc, char** arg){

	int points = 0;

	SpriteManager* sm = new SpriteManager();
	PongGame *game = new PongGame(sm);
	
	if(!game->Initialize())
		std::cout << "Error during initializing!";

	if (!game->GameLoop())
		std::cout << "Error during game!";

	game->CleanUp();

	SDL_Quit();
	return 0;
}