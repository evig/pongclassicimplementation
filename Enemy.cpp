#include "Enemy.h"


Enemy::Enemy(int spriteID, int w, int h, Actor* target)
	: Actor(spriteID, w, h){
	m_target = target;
}

void Enemy::Update(){
	m_position.Y = 20;
	if (m_target == nullptr)
		return;

	m_velocity.X = (m_target->GetCenter().X - GetCenter().X)*0.025f;

	m_position += m_velocity;
}