#include "SpriteManager.h"
#include "Helpers.h"
#include <time.h>

SpriteManager::SpriteManager(SDL_Renderer *renderer, SDL_Texture* texture){

	this->m_renderer = renderer;
	this->m_texture = texture;
}
Sprite SpriteManager::GetSpriteData(int ID){
	if (m_spritesheet.size() < 1 || ID < 0 || ID > m_spritesheet.size() - 1)
		return Sprite();

	return m_spritesheet[ID];
}
void SpriteManager::AddSpriteData(int x, int y, int w, int h){
	m_spritesheet.push_back(Sprite(x, y, w, h));
}
void SpriteManager::RenderSprite(int x, int y, int ID, bool flip){
	if (m_renderer == nullptr || m_texture == nullptr)
		return;

	Sprite sprite = GetSpriteData(ID);

	SDL_Rect rect;
	rect.x = x;
	rect.y = y;
	rect.w = sprite.w;
	rect.h = sprite.h;

	SDL_Rect clip;
	clip.x = sprite.sheet_x;
	clip.y = sprite.sheet_y;
	clip.w = sprite.w;
	clip.h = sprite.h;
	
	SDL_RenderCopyEx(m_renderer, m_texture, &clip, &rect, 0, new SDL_Point(), (flip ? SDL_FLIP_VERTICAL : SDL_FLIP_NONE));
}