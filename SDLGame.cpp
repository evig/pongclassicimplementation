#include "SDLGame.h"


SDLGame::SDLGame(SpriteManager* sm){
	this->m_sprites = sm;
}
bool SDLGame::Initialize(){

	srand(time(NULL));

	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return false;
	}


	m_wnd = SDL_CreateWindow("Pong", 50, 50, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);

	if (m_wnd == nullptr){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return false;
	}

	m_renderer = SDL_CreateRenderer(m_wnd, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (m_renderer == nullptr){
		SDL_DestroyWindow(m_wnd);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return false;
	}

	if (!(IMG_Init(IMG_INIT_PNG)))
	{
		printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
		return false;
	}


	std::string filename = "pong_main_001.png";
	std::string path = SDL_GetBasePath();
	std::string fullpath = path + filename;

	SDL_Surface *bmp = IMG_Load(fullpath.c_str());
	if (bmp == nullptr){
		SDL_DestroyRenderer(m_renderer);
		SDL_DestroyWindow(m_wnd);
		std::cout << "SDL_LoadBMP Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}


	m_texture = SDL_CreateTextureFromSurface(m_renderer, bmp);
	SDL_FreeSurface(bmp);
	if (m_renderer == nullptr){
		SDL_DestroyRenderer(m_renderer);
		SDL_DestroyWindow(m_wnd);
		std::cout << "SDL_CreateTextureFromSurface Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}


	//Initialize SDL_ttf
	if (TTF_Init() == -1)
	{
		printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
		return false;
	}

	// Load a font
	m_font = TTF_OpenFont("Aller_Rg.ttf", 24);
	if (m_font == NULL)
	{
		std::cout << "TTF_OpenFont() Failed: " << TTF_GetError() << std::endl;
		TTF_Quit();
		SDL_Quit();
		return false;
	}
}

void SDLGame::UpdateLogic(){

}
void SDLGame::AfterUpdateLogic(){ }
void SDLGame::Draw(){

}
void SDLGame::AfterDraw(){ }

bool SDLGame::GameLoop(){


	return true;
}

void SDLGame::CleanUp(){
	for (size_t i = 0; i < m_actors.size(); i++)
	{
		delete m_actors[i];
	}

	SDL_Quit();
}