#ifndef ENEMY_H
#define ENEMY_H

#include "Actor.h"

// Enemy actor class
class Enemy : public Actor{
protected:
	Actor*		m_target;

public:
	Enemy(int spriteID, int w, int h, Actor* target);

	// This methods implements following after ball
	void Update();
};

#endif