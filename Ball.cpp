#include "Ball.h"


Ball::Ball(int spriteID, int w, int h)
	: Actor(spriteID, w, h){
	m_velocity = Vector2(3, 1);
}

void Ball::Reset(bool initVelocity){

	if (initVelocity)
		m_velocity = Vector2(Helper::random(-5, 5), Helper::random(-10, 10));
	else
		m_velocity = Vector2(0, 0);

	m_position = Vector2(WINDOW_WIDTH*0.5f, WINDOW_HEIGHT*0.5f);
}

void Ball::Update(){

	m_position += m_velocity;

	if (m_position.X < 0){
		m_position.X = 0;
		m_velocity.X = -m_velocity.X;
	}
	if (m_position.X > WINDOW_WIDTH){
		m_position.X = WINDOW_WIDTH;
		m_velocity.X = -m_velocity.X;
	}
}
