#ifndef HELPERS_H
#define HELPERS_H

#include <iostream>


#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 800
#define FRAMES 60

// Helper functions in one bag
// - simple math, collisions
// - operators overriding

class Helper{
public:
	static int random(int from, int to);
	static int common(int p1x1, int p1x2, int p2x1, int p2x2);
};



class Vector2{
public:
	float X;
	float Y;

	Vector2();
	Vector2(float x, float y);

	float Length();

	Vector2 Normalized();
};


void operator += (Vector2 &a, Vector2 &b);
Vector2 operator - (Vector2 &a, Vector2 &b);
void operator -= (Vector2 &a, Vector2 &b);
Vector2 operator + (Vector2 &a, Vector2 &b);
Vector2 operator * (Vector2 &a, int value);
void operator *= (Vector2 &a, float value);


enum CollisionType{
	Horizontal,
	Vertical
};

#endif