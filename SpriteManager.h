#ifndef SM_H
#define SM_H

#include <vector>
#include "SDL.h"


struct Sprite{
public:
	int sheet_x = 0;
	int sheet_y = 0;
	int w = 0;
	int h = 0;

	Sprite(int sheet_x, int sheet_y, int w, int h){
		this->sheet_x = sheet_x;
		this->sheet_y = sheet_y;
		this->w = w;
		this->h = h;
	}

	Sprite(){
		this->sheet_x = 0;
		this->sheet_y = 0;
		this->w = 0;
		this->h = 0;
	}
};

// Simple sprite manager wrapping class which holds data loaded from spritesheet given
// Also supports rendering of the sprites using SDL library
class SpriteManager{
private:

	SDL_Texture*		m_texture;
	std::vector<Sprite> m_spritesheet;
	SDL_Renderer*		m_renderer;

public:

	SpriteManager(){ }
	SpriteManager(SDL_Renderer *renderer, SDL_Texture* texture);
	void				AddSpriteData(int x, int y, int w, int h);
	Sprite				GetSpriteData(int ID);
	void				RenderSprite(int x, int y, int ID, bool flipped = false);

};


#endif