#include "Actor.h"


Actor::Actor(int spriteID, int w, int h){
	m_sizes = Vector2(w, h);
	m_spriteID = spriteID;
}



void Actor::Update(){

}
void Actor::UpdatePosition(Vector2 newPosition, bool zeroVelocity){
	this->m_position = newPosition;

	if (!zeroVelocity)
		this->m_velocity = (this->m_position - this->m_lastPosition);
	else
		this->m_velocity = Vector2(0, 0);

	this->m_lastPosition = this->m_position;
}

void Actor::Draw(SpriteManager* sm){
	sm->RenderSprite(m_position.X, m_position.Y, m_spriteID, m_flippedVisual);
}
bool Actor::Collides(Actor &other, CollisionType* type){
	int cx = Helper::common(m_position.X, m_position.X + m_sizes.X, other.GetPosition().X, other.GetPosition().X + other.GetSizes().X);
	int cy = Helper::common(m_position.Y, m_position.Y + m_sizes.Y, other.GetPosition().Y, other.GetPosition().Y + other.GetSizes().Y);

	if (cx < cy)
		*(type) = Horizontal;
	else *(type) = Vertical;

	return (cx > 0 && cy > 0);
}
void Actor::HandleCollision(Actor* other){
	if (other == nullptr)
		return;

	CollisionType type;
	if (this->Collides(*other, &type)){
		Vector2 bump = this->GetVelocity();

		if (type == Vertical)
			bump += Vector2(other->GetVelocity().X, -other->GetVelocity().Y);
		else if (type == Horizontal)
			bump += Vector2(-other->GetVelocity().X, other->GetVelocity().Y);

		other->SetVelocity(bump);
	}
}